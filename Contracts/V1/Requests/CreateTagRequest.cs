namespace ApiPlayground.Controllers.V1
{
    public class CreateTagRequest
    {
        public string TagName { get; set; }
    }
}