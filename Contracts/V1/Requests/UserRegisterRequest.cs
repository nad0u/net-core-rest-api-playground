using System.ComponentModel.DataAnnotations;

namespace ApiPlayground.Contracts.V1.Requests
{
    public class UserRegisterRequest
    {
        [EmailAddress]
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
