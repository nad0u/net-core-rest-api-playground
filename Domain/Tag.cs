using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Identity;

namespace ApiPlayground.Domain
{
    public class Tag
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string CreatorId { get; set; }
        public DateTime CreatedOn { get; set; }
        [JsonIgnore]
        public ICollection<Post> Posts { get; set; }
        
        [ForeignKey(nameof(CreatorId))]
        public IdentityUser CreatedBy { get; set; }
    }
}