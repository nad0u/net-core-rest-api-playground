using System.Linq;
using ApiPlayground.Contracts.V1.Responses;
using ApiPlayground.Domain;
using AutoMapper;

namespace ApiPlayground.MappingProfiles
{
    public class DomainToResponseProfile : Profile
    {
        public DomainToResponseProfile()
        {
            CreateMap<Post, PostResponse>()
                .ForMember(dest => dest.Tags, opt => 
                    opt.MapFrom(src => src.Tags.Select(x => new TagResponse{TagName = x.Name}))); // useful in case we don't have an exact matching property name
            CreateMap<Tag, TagResponse>()
                .ForMember(dest => dest.TagName, opt => 
                    opt.MapFrom(src => src.Name));
        }
        
    }
}