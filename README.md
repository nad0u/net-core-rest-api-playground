# EF Core

## Setup

Install the dotnet-ef tool :
```shell
dotnet tool update --global dotnet-ef
```

Make sure it is added to your `.bashrc` or `.zshrc` or whatever :

```shell
export PATH="$PATH:$HOME/.dotnet/tools"
```

## Commands

Add migration :

```shell
dotnet ef migrations add <MigrationName>
```

Excute/run migration :

```shell
dotnet ef database update
```
