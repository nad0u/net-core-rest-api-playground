using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApiPlayground.Domain;

namespace ApiPlayground.Services
{
    public interface IIdentityService
    {
        Task<AuthenticationResult> RegisterAsync(string email, string password);
        Task<AuthenticationResult> LoginAsync(string email, string password);
        Task<AuthenticationResult> RefreshTokenAsync(string token, string refreshToken);
    }
}
