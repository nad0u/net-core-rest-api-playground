using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApiPlayground.Domain;

namespace ApiPlayground.Services
{
    public interface IPostService
    {
        Task<List<Post>> GetPostsAsync();
        Task<Post> GetPostByIdAsync(Guid postId);
        Task<bool> CreatePostAsync(Post postToCreate);
        Task<bool> CreatePostWithTagsAsync(Post postToCreate, List<Tag> tags);
        Task<bool> UpdatePostAsync(Post postToUpdate);
        Task<bool> DeletePostAsync(Guid postId);
        Task<bool> UserOwnsPostAsync(Guid postId, string userId);
        Task<List<Tag>> GetAllTagsAsync();
        Task<Tag> GetTagByNameAsync(string tagName);
        Task<bool> CreateTagAsync(Tag tagToCreate);
        Task<bool> DeleteTagAsync(string tagName);
    }
}
